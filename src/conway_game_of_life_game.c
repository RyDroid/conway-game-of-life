/**
 * @file
 * 
 * @section license License
 * 
 * Copyright (C) 2014  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#include "conway_game_of_life_game.h"


void conway_game_of_life_init(conway_game_of_life* game)
{
  game->universe = tab_2d_char_create(0, 0);
  game->nb_turns = 0;
}

int conway_game_of_life_next_turn(conway_game_of_life* game)
{
  if(tab_2d_char_is_init(&game->universe))
    {
      game->universe = conway_game_of_life_get_next_turn(&game->universe);
      ++game->nb_turns;
      return CONWAY_GAME_OF_LIFE_ACTION_SUCCESS;
    }
  
  return !CONWAY_GAME_OF_LIFE_ACTION_SUCCESS;
}
