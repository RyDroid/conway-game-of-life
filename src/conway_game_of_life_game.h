/**
 * @file
 * 
 * @section license License
 * 
 * Copyright (C) 2014  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef CONWAY_GAME_OF_LIFE_GAME_H
#define CONWAY_GAME_OF_LIFE_GAME_H

#include "conway_game_of_life_essential.h"


#define CONWAY_GAME_OF_LIFE_ACTION_SUCCESS 0

typedef struct conway_game_of_life conway_game_of_life;

/**
 * A Conway's Game of Life.
 */
struct conway_game_of_life
{
  /**
   * A universe of a Conway's Game of Life.
   */
  tab_2d_char universe;
  
  /**
   * Number of turns.
   */
  unsigned int nb_turns;
};

/**
 * Initialize a Conway's Game of Life.
 * @param game A Conway's Game of Life
 */
void conway_game_of_life_init(conway_game_of_life* game);

/**
 * Compute the next turn of a Conway's Game of Life
 * @param game A Conway's Game of Life
 * @return error Returns 0 if it works, or an error code.
 */
int conway_game_of_life_next_turn(conway_game_of_life* game);

#endif
