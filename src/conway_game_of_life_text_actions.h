/**
 * @file
 * 
 * @section license License
 * 
 * Copyright (C) 2014  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef CONWAY_GAME_OF_LIFE_TEXT_ACTIONS_H
#define CONWAY_GAME_OF_LIFE_TEXT_ACTIONS_H

#include "tab_2d_char_scan.h"
#include "conway_game_of_life_environment.h"


/**
 * An action of a Conway's Game of Life
 */
typedef int (*conway_game_of_life_text_action)(conway_game_of_life_terminal_environment* env);

int conway_game_of_life_print_help(conway_game_of_life_terminal_environment* env);

/**
 * Ask information to the user with text in order to load a universe.
 * @param universe A future universe
 * @return True if a universe is loaded, otherwise false
 */
/**
 * 
 * @param game A Conway's Game of Life
 * @return error Returns 0 if it works, or an error code.
 */
int conway_game_of_life_load_universe(tab_2d_char* universe);

int conway_game_of_life_environment_load_universe(conway_game_of_life_terminal_environment* env);

/**
 * 
 * @param game A Conway's Game of Life
 * @return error Returns 0 if it works, or an error code.
 */
int conway_game_of_life_print_universe(const conway_game_of_life_terminal_environment* env);

/**
 * 
 * @param game A Conway's Game of Life
 * @return error Returns 0 if it works, or an error code.
 */
int conway_game_of_life_print_nb_turns(const conway_game_of_life_terminal_environment* env);

/**
 * 
 * @param game A Conway's Game of Life
 * @return error Returns 0 if it works, or an error code.
 */
int conway_game_of_life_print_nb_lines(const conway_game_of_life_terminal_environment* env);

/**
 * 
 * @param game A Conway's Game of Life
 * @return error Returns 0 if it works, or an error code.
 */
int conway_game_of_life_print_nb_columns(const conway_game_of_life_terminal_environment* env);

/**
 * 
 * @param game A Conway's Game of Life
 * @return error Returns 0 if it works, or an error code.
 */
int conway_game_of_life_print_grid_size(const conway_game_of_life_terminal_environment* env);

int conway_game_of_life_print_debug_status(const conway_game_of_life_terminal_environment* env);

/**
 * 
 * @param game A Conway's Game of Life
 * @return error Returns 0 if it works, or an error code.
 */
int conway_game_of_life_next_turn_print_err(conway_game_of_life_terminal_environment* env);

void conway_game_of_life_terminal_environment_ask_command(conway_game_of_life_terminal_environment* env);

bool conway_game_of_life_terminal_environment_execute_command(conway_game_of_life_terminal_environment* env);

#endif
