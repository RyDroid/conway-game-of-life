/**
 * @file
 * 
 * @section license License
 * 
 * Copyright (C) 2014  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef CONWAY_GAME_OF_LIFE_TERMINAL_ENVIRONMENT_H
#define CONWAY_GAME_OF_LIFE_TERMINAL_ENVIRONMENT_H

#include "string_functions.h"
#include "conway_game_of_life_game.h"


#define ENV_TEXT_COMMAND_LENGTH_MAX 255
#define CONWAY_GAME_OF_LIFE_ACTION_SUCCESS 0

typedef struct conway_game_of_life_terminal_environment conway_game_of_life_terminal_environment;

/**
 * A Conway's Game of Life.
 */
struct conway_game_of_life_terminal_environment
{
  
  /**
   * A Conway's Game of Life.
   */
  conway_game_of_life game;

  /**
   * Current text command
   */
  char current_text_command[ENV_TEXT_COMMAND_LENGTH_MAX];
  
  /**
   * Debug?
   */
  bool debug;
  
  /**
   * Raise an error for and empty command?
   */
  bool error_for_empty_command;
};

void conway_game_of_life_terminal_environment_init(conway_game_of_life_terminal_environment* env);

#endif
