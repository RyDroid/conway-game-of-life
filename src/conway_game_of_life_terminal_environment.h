/**
 * @file
 * 
 * @section license License
 * 
 * Copyright (C) 2014  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef CONWAY_GAME_OF_LIFE_ENVIRONMENT_H
#define CONWAY_GAME_OF_LIFE_ENVIRONMENT_H

#include "conway_game_of_life.h"


#define CONWAY_GAME_OF_LIFE_ACTION_SUCCESS 0

typedef struct conway_game_of_life conway_game_of_life;

/**
 * A Conway's Game of Life.
 */
struct conway_game_of_life_terminal_environment
{
  /**
   * Current text command
   */
  char* current_text_command;
  
  /**
   * Debug?
   */
  bool debug;
  
  /**
   * A Conway's Game of Life.
   */
  conway_game_of_life game;
};

#endif
