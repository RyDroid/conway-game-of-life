/**
 * @file
 * 
 * @section license License
 * 
 * Copyright (C) 2014  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#include "conway_game_of_life_text_actions.h"


int conway_game_of_life_print_help(conway_game_of_life_terminal_environment* env)
{
  puts("h|help -- Print memo of commands.");
  puts("q|quit -- Quit the program.");
  puts("l|load -- Load a universe.");
  puts("n|next -- Compute next turn of the universe.");
  puts("p|print -- Print the current universe.");
  puts("nb_turns -- Print the current turn number.");
  puts("grid_size -- Print the size of the grid.");
  puts("nb_lines -- Print the number of lines.");
  puts("nb_columns -- Print the number of columns.");
  puts("debug_status -- Print the status of debugging.");
  puts("debug_on -- Put debug on.");
  puts("debug_off -- Put debug off.");
  return CONWAY_GAME_OF_LIFE_ACTION_SUCCESS;
}

bool conway_game_of_life_load_universe(tab_2d_char* universe)
{
  if(universe == NULL)
    {
      fprintf(stderr, "ask_load_universe => universe == NULL\n");
      return false;
    }
  
  tab_2d_char_destruct(universe);
  
  {
    printf("File path of the universe to load: ");
    char file_path[2000];
    fgets(file_path, 2000, stdin);
    if(isspace(file_path[strlen(file_path)-1]) || iscntrl(file_path[strlen(file_path)-1]))
      file_path[strlen(file_path)-1] = '\0';
    
    if(strlen(file_path) == 0)
      tab_2d_char_scan_stdin(universe);
    else
      tab_2d_char_get_from_file_path(universe, file_path);
  }
  
  if(conway_game_of_life_is_universe(universe))
    return true;
  
  fprintf(stderr, "There is no universe. :(\n");
  return false;
}

int conway_game_of_life_environment_load_universe(conway_game_of_life_terminal_environment* env)
{
  if(conway_game_of_life_load_universe(&env->game.universe))
    {
      env->game.nb_turns = 0;
      return CONWAY_GAME_OF_LIFE_ACTION_SUCCESS;
    }
  
  env->game.universe.tab = NULL;
  return !CONWAY_GAME_OF_LIFE_ACTION_SUCCESS;
}

int conway_game_of_life_print_universe(const conway_game_of_life_terminal_environment* env)
{
  if(tab_2d_char_is_init(&env->game.universe))
    tab_2d_char_print_stdout_without_grid(&env->game.universe);
  else
    fprintf(stderr, "There is no universe. :(\n");
  
  return CONWAY_GAME_OF_LIFE_ACTION_SUCCESS;
}

int conway_game_of_life_print_nb_turns(const conway_game_of_life_terminal_environment* env)
{
  printf("Turn %u\n", env->game.nb_turns);
  return CONWAY_GAME_OF_LIFE_ACTION_SUCCESS;
}

int conway_game_of_life_print_nb_lines(const conway_game_of_life_terminal_environment* env)
{
  printf("Number of lines %u\n", env->game.universe.nb_lines);
  return CONWAY_GAME_OF_LIFE_ACTION_SUCCESS;
}

int conway_game_of_life_print_nb_columns(const conway_game_of_life_terminal_environment* env)
{
  printf("Number of columns %u\n", env->game.universe.nb_columns);
  return CONWAY_GAME_OF_LIFE_ACTION_SUCCESS;
}

int conway_game_of_life_print_grid_size(const conway_game_of_life_terminal_environment* env)
{
  printf("%u", env->game.universe.nb_lines);
  fputs(" line", stdout);
  if(env->game.universe.nb_lines > 1)
    fputc('s', stdout);
  fputs(" and ", stdout);
  printf("%u", env->game.universe.nb_columns);
  fputs(" column", stdout);
  if(env->game.universe.nb_columns > 1)
    fputc('s', stdout);
  puts("");
  
  return CONWAY_GAME_OF_LIFE_ACTION_SUCCESS;
}

int conway_game_of_life_print_debug_status(const conway_game_of_life_terminal_environment* env)
{
  if(env->debug)
    fputs("Debug enabled.", stdout);
  else
    fputs("Debug disabled.", stdout);
  puts("");
  return CONWAY_GAME_OF_LIFE_ACTION_SUCCESS;
}

int conway_game_of_life_next_turn_print_err(conway_game_of_life_terminal_environment* env)
{
  int error_code = conway_game_of_life_next_turn(&env->game);
  
  if(error_code == CONWAY_GAME_OF_LIFE_ACTION_SUCCESS)
    {
      if(env->debug)
	{
	  char file_path[6 + 1 + env->game.nb_turns / 1];
	  sprintf(file_path, "debug-%u", env->game.nb_turns);
	  tab_2d_char_save_to_file_path(&env->game.universe, file_path);
	}

      if(conway_game_of_life_is_universe_over(&env->game.universe))
	{
	  env->current_text_command[0] = 'o';
	  env->current_text_command[1] = 'v';
	  env->current_text_command[2] = 'e';
	  env->current_text_command[3] = 'r';
	  env->current_text_command[4] = '\0';
	}
    }
  else
    fprintf(stderr, "There is no universe. :(\n");
  
  return error_code;
}

void conway_game_of_life_terminal_environment_ask_command(conway_game_of_life_terminal_environment* env)
{
  //flush_stdin();
  fputs("Command: ", stdout);
  fgets(env->current_text_command, ENV_TEXT_COMMAND_LENGTH_MAX, stdin);
  size_t cmd_len = strlen(env->current_text_command);
  if(isspace(env->current_text_command[cmd_len-1]) || iscntrl(env->current_text_command[cmd_len-1]))
    env->current_text_command[strlen(env->current_text_command)-1] = '\0';
  string_trim(env->current_text_command, ' ');
  string_tolower(env->current_text_command);
}

bool conway_game_of_life_terminal_environment_execute_command(conway_game_of_life_terminal_environment* env)
{
  if(string_equals(env->current_text_command, "h") || string_equals(env->current_text_command, "help") || string_equals(env->current_text_command, "commands") || string_equals(env->current_text_command, "aide"))
    {
      conway_game_of_life_print_help(env);
    }
  else if
    (
     string_equals(env->current_text_command, "q") ||
     string_equals(env->current_text_command, "quit") ||
     string_equals(env->current_text_command, "k") ||
     string_equals(env->current_text_command, "kill") ||
     string_equals(env->current_text_command, "exit") ||
     string_equals(env->current_text_command, "close") ||
     string_equals(env->current_text_command, "over") ||
     string_equals(env->current_text_command, "good bye")
     )
    {
      tab_2d_char_destruct(&env->game.universe);
      return false;
    }
  else if(string_equals(env->current_text_command, "l") || string_equals(env->current_text_command, "load"))
    {
      conway_game_of_life_environment_load_universe(env);
    }
  else if(string_equals(env->current_text_command, "c") || string_equals(env->current_text_command, "continue") || string_equals(env->current_text_command, "n") || string_equals(env->current_text_command, "next"))
    {
      if(tab_2d_char_is_init(&env->game.universe))
	{
	  env->game.universe = conway_game_of_life_get_next_turn(&env->game.universe);
	  ++env->game.nb_turns;
      
	  if(env->debug)
	    {
	      char file_path[6 + 1 + env->game.nb_turns / 1];
	      sprintf(file_path, "debug-%u", env->game.nb_turns);
	      tab_2d_char_save_to_file_path(&env->game.universe, file_path);
	    }

	  if(conway_game_of_life_is_universe_over(&env->game.universe))
	    {
	      env->current_text_command[0] = 'o';
	      env->current_text_command[1] = 'v';
	      env->current_text_command[2] = 'e';
	      env->current_text_command[3] = 'r';
	      env->current_text_command[4] = '\0';
	    }
	}
      else
	fprintf(stderr, "There is no universe. :(\n");
    }
  else if(string_equals(env->current_text_command, "p") || string_equals(env->current_text_command, "print") || string_equals(env->current_text_command, "display"))
    {
      conway_game_of_life_print_universe(env);
    }
  else if(string_equals(env->current_text_command, "nb_turns"))
    {
      conway_game_of_life_print_nb_turns(env);
    }
  else if(string_equals(env->current_text_command, "grid_size"))
    {
      conway_game_of_life_print_grid_size(env);
    }
  else if(string_equals(env->current_text_command, "nb_lines"))
    {
      conway_game_of_life_print_nb_lines(env);
    }
  else if(string_equals(env->current_text_command, "nb_columns"))
    {
      conway_game_of_life_print_nb_columns(env);
    }
  else if(string_equals(env->current_text_command, "debug_status"))
    {
      conway_game_of_life_print_debug_status(env);
    }
  else if(string_equals(env->current_text_command, "debug_on"))
    {
      env->debug = true;
    }
  else if(string_equals(env->current_text_command, "debug_off"))
    {
      env->debug = false;
    }
  else if(*env->current_text_command == '\0')
    {
      if(env->error_for_empty_command)
	{
	  fputs("Empty command.", stderr);
	  puts("");
	}
    }
  else
    {
      fputs("Unknown command.", stderr);
      puts("");
    }
  
  return true;
}
