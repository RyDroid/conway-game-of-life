/*
 * Copyright (C) 2014-2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#include "tab_2d_char_essential.h"


void
tab_2d_char_alloc(tab_2d_char* tab_2d)
{
  if(tab_2d_char_is_init(tab_2d))
    {
      free(tab_2d->tab);
    }
  tab_2d_char_alloc_unsafe(tab_2d);
}

void
tab_2d_char_init_unsafe(tab_2d_char* tab_2d, unsigned int nb_lines, unsigned int nb_columns)
{
  tab_2d->nb_lines = nb_lines;
  tab_2d->nb_columns = nb_columns;
  tab_2d_char_alloc_unsafe(tab_2d);
}

void
tab_2d_char_init_one_time_only(tab_2d_char* tab_2d, unsigned int nb_lines, unsigned int nb_columns)
{
  if(tab_2d != NULL)
    {
      tab_2d_char_init_unsafe(tab_2d, nb_lines, nb_columns);
    }
}

void
tab_2d_char_init(tab_2d_char* tab_2d, unsigned int nb_lines, unsigned int nb_columns)
{
  if(!tab_2d_char_is_init(tab_2d))
    {
      tab_2d_char_init_unsafe(tab_2d, nb_lines, nb_columns);
    }
}

tab_2d_char
tab_2d_char_create(unsigned int nb_lines, unsigned int nb_columns)
{
  tab_2d_char tab_2d = {NULL, 0, 0};
  tab_2d_char_init_unsafe(&tab_2d, nb_lines, nb_columns);
  return tab_2d;
}

void
tab_2d_char_destruct(tab_2d_char* tab_2d)
{
  TAB_2D_GENERIC_STATIC_POINTER_DESTRUCT(tab_2d);
}

bool
tab_2d_char_line_exists(const tab_2d_char* tab_2d, unsigned int line)
{
  return TAB_2D_GENERIC_STATIC_POINTER_LINE_EXISTS(tab_2d, line);
}

bool
tab_2d_char_column_exists(const tab_2d_char* tab_2d, unsigned int column)
{
  return TAB_2D_GENERIC_STATIC_POINTER_COLUMN_EXISTS(tab_2d, column);
}

bool
tab_2d_char_element_exists(const tab_2d_char* tab_2d, unsigned int line, unsigned int column)
{
  return TAB_2D_GENERIC_STATIC_POINTER_ELEMENT_EXISTS(tab_2d, line, column);
}

char*
tab_2d_char_get_element_pointer(const tab_2d_char* tab_2d, unsigned int line, unsigned int column)
{
  return
    tab_2d_char_element_exists(tab_2d, line, column)
    ? tab_2d_char_get_element_pointer_unsafe(tab_2d, line, column)
    : NULL;
}

char tab_2d_char_get_element_value(const tab_2d_char* tab_2d, unsigned int line, unsigned int column)
{
  char * pointer = tab_2d_char_get_element_pointer(tab_2d, line, column);
  return pointer == NULL ? '\0' : *pointer;
}

void
tab_2d_char_set_element_value(tab_2d_char* tab_2d, unsigned int line, unsigned int column, char value)
{
  char * pointer = tab_2d_char_get_element_pointer(tab_2d, line, column);
  if(pointer != NULL)
    {
      *pointer = value;
    }
}

void
tab_2d_char_fill_with_const(tab_2d_char* tab_2d, char value_for_filling)
{
  if(tab_2d_char_is_init(tab_2d))
    {
      unsigned int line, column;
      for(line=0; line < tab_2d->nb_lines; ++line)
	{
	  for(column=0; column < tab_2d->nb_columns; ++column)
	    {
	      tab_2d_char_set_element_value_unsafe(tab_2d, line, column, value_for_filling);
	    }
	}
    }
}
