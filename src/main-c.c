/**
 * Copyright (C) 2014  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author Nicola Spanti (RyDroid)
 */


#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>

#include "conway_game_of_life_text_actions.h"

#define USER_ANSWER_LENGTH_MAX 255


int main(int argc, char* argv[])
{
  conway_game_of_life_terminal_environment env;
  conway_game_of_life_terminal_environment_init(&env);
  
  
  if(argc > 1)
    {
      if(string_equals(argv[1], "-h") || string_equals(argv[1], "--help") || string_equals(argv[1], "--aide"))
	{
	  puts("A free/libre Conway's Game of Life");
	  conway_game_of_life_print_help(&env);
	  return EXIT_SUCCESS;
	}
      if(string_equals(argv[1], "--license") || string_equals(argv[1], "--licence"))
	{
	  puts("This program is under GNU Lesser General Public License 3 as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.");
	  return EXIT_SUCCESS;
	}
      if(string_equals(argv[1], "--author") || string_equals(argv[1], "--auteur"))
	{
	  puts("The author of this program is Nicola Spanti, also known as RyDroid.");
	  return EXIT_SUCCESS;
	}
      if(string_equals(argv[1], "-g") || string_equals(argv[1], "--debug"))
	env.debug = true;
    }
  
  
  puts("A free/libre Conway's Game of Life");
  puts("Use 'help' command if you need it.");
  puts("");
  
  /*unsigned int number_of_lines, number_of_columns;
  printf("Number of lines : ");
  scanf("%u", &number_of_lines);
  printf("Number of columns : ");
  scanf("%u", &number_of_columns);
  
  tab_2d_char universe = tab_2d_char_create(number_of_lines, number_of_columns);
  tab_2d_char_print_stdout_without_grid(&square);
  TODO Generate randomly*/
  
  bool go_on = true;
  while(go_on)
    {
      conway_game_of_life_terminal_environment_ask_command(&env);
      go_on = conway_game_of_life_terminal_environment_execute_command(&env);
    }
  
  return EXIT_SUCCESS;
}
