# Copyright (C) 2014-2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


SRC_DIR=src
BIN_DIR=bin
DOC_DIR=doc
DOXYGEN_FILE=etc/doxygen_configuration.ini

CC=gcc
INCLUDES=-I./$(SRC_DIR)
DEBUG_FLAGS=-O0 -g
CFLAGS=-std=c99 -Wall -Wextra -Wpedantic -O2 $(INCLUDES) $(DEBUG_FLAGS) -fPIC
RM=rm -f

PACKAGE=conway-game-of-life
FILES_TO_ARCHIVE=$(SRC_DIR)/ makefile etc/ LICENSE* mainpage.dox tests/ .gitignore .editorconfig


.PHONY: $(DOC_DIR)

all: $(BIN_DIR)/main-c $(BIN_DIR)/libtab2dchar.a $(BIN_DIR)/libtab2dchar.so doc tar-bz2


$(BIN_DIR)/main-c: $(SRC_DIR)/bool.h $(BIN_DIR)/stdio_functions.o $(BIN_DIR)/string_functions.o $(BIN_DIR)/tab_2d_char_essential.o $(BIN_DIR)/tab_2d_char_print.o $(BIN_DIR)/tab_2d_char_scan.o $(BIN_DIR)/tab_2d_char_file.o $(BIN_DIR)/conway_game_of_life_essential.o $(BIN_DIR)/conway_game_of_life_game.o $(BIN_DIR)/conway_game_of_life_environment.o $(BIN_DIR)/conway_game_of_life_text_actions.o $(BIN_DIR)/main-c.o
	$(CC) $(CFLAGS) \
		$(BIN_DIR)/stdio_functions.o $(BIN_DIR)/string_functions.o \
		$(BIN_DIR)/tab_2d_char_essential.o $(BIN_DIR)/tab_2d_char_print.o $(BIN_DIR)/tab_2d_char_scan.o $(BIN_DIR)/tab_2d_char_file.o \
		$(BIN_DIR)/conway_game_of_life_essential.o $(BIN_DIR)/conway_game_of_life_game.o $(BIN_DIR)/conway_game_of_life_environment.o $(BIN_DIR)/conway_game_of_life_text_actions.o \
		$(BIN_DIR)/main-c.o \
		-o $(BIN_DIR)/main-c

$(BIN_DIR)/main-c.o: $(SRC_DIR)/tab_2d_char_file.h $(SRC_DIR)/conway_game_of_life_environment.h $(SRC_DIR)/main-c.c
	@mkdir -p $(BIN_DIR)/
	$(CC) $(CFLAGS) -c $(SRC_DIR)/main-c.c -o $(BIN_DIR)/main-c.o

$(BIN_DIR)/conway_game_of_life_text_actions.o: $(SRC_DIR)/conway_game_of_life_environment.h $(SRC_DIR)/conway_game_of_life_text_actions.h $(SRC_DIR)/conway_game_of_life_text_actions.c
	@mkdir -p $(BIN_DIR)/
	$(CC) $(CFLAGS) -c $(SRC_DIR)/conway_game_of_life_text_actions.c -o $(BIN_DIR)/conway_game_of_life_text_actions.o

$(BIN_DIR)/conway_game_of_life_environment.o: $(SRC_DIR)/string_functions.h $(SRC_DIR)/conway_game_of_life_game.h $(SRC_DIR)/conway_game_of_life_environment.h $(SRC_DIR)/conway_game_of_life_environment.c
	@mkdir -p $(BIN_DIR)/
	$(CC) $(CFLAGS) -c $(SRC_DIR)/conway_game_of_life_environment.c -o $(BIN_DIR)/conway_game_of_life_environment.o

$(BIN_DIR)/conway_game_of_life_game.o: $(SRC_DIR)/conway_game_of_life_essential.h $(SRC_DIR)/conway_game_of_life_game.h $(SRC_DIR)/conway_game_of_life_game.c
	@mkdir -p $(BIN_DIR)/
	$(CC) $(CFLAGS) -c $(SRC_DIR)/conway_game_of_life_game.c -o $(BIN_DIR)/conway_game_of_life_game.o

$(BIN_DIR)/conway_game_of_life_essential.o: $(SRC_DIR)/tab_2d_char_print.h $(SRC_DIR)/conway_game_of_life_essential.h $(SRC_DIR)/conway_game_of_life_essential.c
	@mkdir -p $(BIN_DIR)/
	$(CC) $(CFLAGS) -c $(SRC_DIR)/conway_game_of_life_essential.c -o $(BIN_DIR)/conway_game_of_life_essential.o

$(BIN_DIR)/libtab2dchar.so: $(BIN_DIR)/stdio_functions.o $(BIN_DIR)/tab_2d_char_essential.o $(BIN_DIR)/tab_2d_char_print.o $(BIN_DIR)/tab_2d_char_scan.o $(BIN_DIR)/tab_2d_char_file.o
	$(CC) -o $(BIN_DIR)/libtab2dchar.so -shared $(BIN_DIR)/tab_2d_char_*.o

$(BIN_DIR)/libtab2dchar.a: $(BIN_DIR)/stdio_functions.o $(BIN_DIR)/tab_2d_char_essential.o $(BIN_DIR)/tab_2d_char_print.o $(BIN_DIR)/tab_2d_char_scan.o $(BIN_DIR)/tab_2d_char_file.o
	ar -rv $(BIN_DIR)/libtab2dchar.a $(BIN_DIR)/tab_2d_char_*.o

$(BIN_DIR)/tab_2d_char_file.o: $(SRC_DIR)/bool.h $(SRC_DIR)/tab_2d_char_file.h $(SRC_DIR)/tab_2d_char_file.h $(SRC_DIR)/tab_2d_char_file.c
	@mkdir -p $(BIN_DIR)/
	$(CC) $(CFLAGS) -c $(SRC_DIR)/tab_2d_char_file.c -o $(BIN_DIR)/tab_2d_char_file.o

$(BIN_DIR)/tab_2d_char_scan.o: $(SRC_DIR)/stdio_functions.h $(SRC_DIR)/tab_2d_char_essential.h $(SRC_DIR)/tab_2d_char_scan.h $(SRC_DIR)/tab_2d_char_scan.c
	$(CC) $(CFLAGS) -c $(SRC_DIR)/tab_2d_char_scan.c -o $(BIN_DIR)/tab_2d_char_scan.o

$(BIN_DIR)/tab_2d_char_print.o: $(SRC_DIR)/tab_2d_char_essential.h $(SRC_DIR)/tab_2d_char_print.h $(SRC_DIR)/tab_2d_char_print.c
	@mkdir -p $(BIN_DIR)/
	$(CC) $(CFLAGS) -c $(SRC_DIR)/tab_2d_char_print.c -o $(BIN_DIR)/tab_2d_char_print.o

$(BIN_DIR)/tab_2d_char_essential.o: $(SRC_DIR)/bool.h $(SRC_DIR)/tab_2d_generic_static.h $(SRC_DIR)/tab_2d_char_essential.h $(SRC_DIR)/tab_2d_char_essential.c
	@mkdir -p $(BIN_DIR)/
	$(CC) $(CFLAGS) -c $(SRC_DIR)/tab_2d_char_essential.c -o $(BIN_DIR)/tab_2d_char_essential.o

$(BIN_DIR)/string_functions.o: $(SRC_DIR)/string_functions.h $(SRC_DIR)/string_functions.c
	@mkdir -p $(BIN_DIR)/
	$(CC) $(CFLAGS) -c $(SRC_DIR)/string_functions.c -o $(BIN_DIR)/string_functions.o

$(BIN_DIR)/stdio_functions.o: $(SRC_DIR)/stdio_functions.h $(SRC_DIR)/stdio_functions.c
	@mkdir -p $(BIN_DIR)/
	$(CC) $(CFLAGS) -c $(SRC_DIR)/stdio_functions.c -o $(BIN_DIR)/stdio_functions.o


$(DOC_DIR): $(SRC_DIR)/*.h $(DOXYGEN_FILE)
	doxygen $(DOXYGEN_FILE)
	@mkdir -p $(DOC_DIR)/
	# PDF with LaTeX
	cd $(DOC_DIR)/latex/ && make


archives: zip tar-gz tar-bz2 tar-xz 7z

zip: $(FILES_TO_ARCHIVE)
	zip $(PACKAGE).zip -r -- $(FILES_TO_ARCHIVE)

tar-gz: $(FILES_TO_ARCHIVE)
	tar -zcvf $(PACKAGE).tar.gz $(FILES_TO_ARCHIVE)

tar-bz2: $(FILES_TO_ARCHIVE)
	tar -jcvf $(PACKAGE).tar.bz2 $(FILES_TO_ARCHIVE)

tar-xz: $(FILES_TO_ARCHIVE)
	tar -cJvf $(PACKAGE).tar.xz $(FILES_TO_ARCHIVE)

7z: $(FILES_TO_ARCHIVE)
	7z a -t7z $(PACKAGE).7z $(FILES_TO_ARCHIVE)


clean:
	$(RM) -r -- \
		$(BIN_DIR)/ $(DOC_DIR)/ \
		*.o *.out *.deb *.rpm *.exe *.msi *.dmg *.jar *.apk *.iso \
		*.zip *.tar.* *.tgz *.7z *.gz *.bz2 *.lz *.lzma *.xz \
		*~ .\#* \#* *.bak *.backup *.sav *.save *.autosav *.autosave \
		$(SRC_DIR)/*~ $(SRC_DIR)/*.bak $(SRC_DIR)/*.save $(SRC_DIR)/*.autosave \
		*.log *.log.* error_log* .*rc .cache/ .thumbnails/ .bash* .zsh* .R* .octave* .xsession*
